#!/usr/bin/env bash

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

export RTE_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

. ${RTE_PATH}/src/ENV.sh
. ${RTE_PATH}/src/functions_and_aliases.sh
. ${RTE_PATH}/src/prompt.sh
. ${RTE_PATH}/src/shopt.sh

# Init fuck
eval $(thefuck --alias --enable-experimental-instant-mode)
