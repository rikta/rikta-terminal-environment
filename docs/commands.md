# Provided Commands

These are the comments provided by this repo.

**I highly suggest to review the code, the most is fairly understandable!

**Especially the `rm` and `nano` alias could may have undesired effects**

* `ll`
    * shows everything except `.` and `..`
    * uses color if supported
    * shows suffixes for types (`/` = directory, `*` = executable, etc.)
    * shows human-readable units of sizes
    * uses the list format 
* `lr`
    * like `ll`, but **with recursion**
* `ld`
    * like `ll`, but **shows only directories**
* `ll`
    * like `lr`+`ld`: **with recursion, shows only directories**
* `..` 
    * `cd ../`
* `...` 
    * `cd ../../`
* `....` 
    * `cd ../../../`
* `mkcd <dirpath>`
    * creates a directory and `cd`s into it
    * creates missing parent directories as well 
* `mvcd <source-dir> <target-dir>`
    * moves something from `source-dir` to `target-dir`
    * if `target-dir` is a directory it `cd`s into it
* `backup <path>`
    * creates backup of a file/directory creating a copy with `.backup`-suffix
* `rm`
    * now automatically use recursion and remove dirs
    * asks if you delete more than 3 files or directories (you can still use `rm -f` to suppress the prompts)
    * tells you what it deleted 
* `rme`
    * recursively remove all empty directories
* `immutable`
    * makes a file immutable, even root can't mutate it (without executing the mutable-command beforehand)
* `mutable` 
    * makes a file mutable again (see `immutable`)
* `underscore`
    * replaces all spaces in the filenames of the current directory with underscores
* `recent`
    * shows recent modified files in this dir and subdirectories sorted by last modification date 
* `hg <searchterm>`
    * searches the history for a command
    * it is handy in combination with `!<history-number>` ;)   
* `please`
    * runs the last command with `sudo`
* `contributors`
    * shows all contributors of a git repository
* `pubkey`
    * show public ssh-key
* `reload`
    * reloads the .bashrc
    * only usefull after changes ;)
* `mkdir`
    * now create missing parent directories if necessary 
    * now reports every directory created
* `make`
    * now uses 4 cores 
* `nano`
    * creates a backup of each file (with suffix `~`)
    * uses spaces instead of tabs
    * scrolls line-wise instead of half-pages
    * sets the size of a tab to 4 spaces
    * always show the cursor position on screen
    * enable automatic indentation
    * enable mouse-support
* `wget`
    * wget now continues after connection errors
* `8888`
    * ping the google-dns server to check if you are online
* `install`
    * installs something via apt-get, but without prompts and quiter
* `fuck`
    * automagically corrects your last command
* `sys`
    * shows the system specification of your system

    
### less useful stuff    

* `weather` 
    * shows a nice in-terminal forecast for berlin
* `online <target>`
    * ping an ip every 10 seconds and ring the alert-bell when it responds
* `say <language> <text...>` 
    * text-to-speech for every installed language
    * l
* `report` 
    * prints the state of the last command with a stegosaurus or a daemon, depending on exit code
    * usefull to end a long-running command where you may leave your pc and look to the screen from the other side of the room for a while
    * use `;`, not `&&` between your command and `report`, or it won't run if it fails!
* `mostUsedCommands`
    * shows the 30 most used commands, sorted by usage
* `bofh`
    * displays a [bofh](https://de.wikipedia.org/wiki/Bastard_Operator_From_Hell)-excuse" 
    * uses telnet, clears the display <.<
* `rancommit`
    * commit with a random message
    * **don't use this at work!!!** ;)
* `all`
    * shows all executable commands
    * sorted alphanumeric
    * useful in combination with grep: `all | grep <something in the middle of a command you dont remember from beginning>`
* `ram`
    * shows the content of the ram
    * I don't know who will ever actually need this ...
* `po`
    * let the pc say "Ask my product manager"
