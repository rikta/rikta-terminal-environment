# Environment Variables

Following environment variables are set:
* `HISTFILESIZE=20000`
    * Maximum number of entries in the history-files
* `HISTSIZE=10000`
    * Maximum number of the entries in the history accessible from the tty
* `HISTCONTROL=ignorespace,ignoredups,erasedups`
    * Ignore subsequent duplicates and erase them, ignore commands starting with a space
* `HISTTIMEFORMAT="%F %T "`
    * Add timestamps to your history
* `HISTIGNORE="&:ls:[bf]g:exit:history -d*"`
    * don't save commands matching this pattern in history
* `EDITOR=nano`
    * sets your default in-terminal-editor
* `CDPATH=:..:~:~/code/:/`
    * sets the path a `cd`-command  will use for search of the directory
    * entries are seperated by `:`
    * with the default of the repo (`:..:~:~/code/:/`) cd will search for the directory from (in following priority):
        * the current directory *(empty string between the equals-sign and the first `:`)*
        * the parent directory
        * your home-directory
        * the folder `code` in your home directory
        * in the root-directory (`/`)  
        * in the root-directory (`/`)  
