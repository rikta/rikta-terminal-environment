#!/usr/bin/env bash

function install() {
    sudo apt-get install $1 -yqq
}

# dependencies of the dependencies
install python3-pip
install python3-dev

# dependencies
pip3 install thefuck # for the fuck-command
install espeak # for the say-command
install cowsay # for the report-command