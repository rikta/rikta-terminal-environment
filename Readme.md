# Riktas Terminal Environment

A collection of several aliases, functions and settings for the bash.

## SETUP

1. clone this repo.  
2. execute `./install.sh`, which will install the dependencies 
3. source the `./index.sh` in your `~/.bashrc` (with absolute path)
4. **check the [OVERRIDES](#OVERRIDES) section below and may adjust the code!!!** 

## Documentation

* [Commands](./docs/commands.md) 
* [Environment Variables](./docs/env.md)
* [Custom Prompt](./docs/prompt.md)
* [Shell Options](./docs/shopt.md)
* [Tips](./docs/shopt.md)

# OVERRIDES

**These aliases will override the behavior you used to!
You should check the behavior below and may adjust them to your needs in the code!**

* `mkdir`
    * now create missing parent directories if necessary 
    * now reports every directory created
* `rm`
    * now automatically use recursion and remove dirs
    * asks if you delete more than 3 files or directories (you can still use `rm -f` to suppress the prompts)
    * tells you what it deleted 
* `make`
    * now uses 4 cores 
* `nano`
    * creates a backup of each file (with suffix `~`)
    * uses spaces instead of tabs
    * scrolls line-wise instead of half-pages
    * sets the size of a tab to 4 spaces
    * always show the cursor position on screen
    * enable automatic indentation
    * enable mouse-support
* `ll`
    * shows everything except `.` and `..`
    * uses color if supported
    * shows suffixes for types (`/` = directory, `*` = executable, etc.)
    * shows human-readable units of sizes
    * uses the list format 
* `wget`
    * wget now continues after connection errors
